import * as React from 'react';
import { Link } from 'react-router-dom';
import { Box, Text } from 'grommet';
import { useSelector, useDispatch } from 'react-redux';
import { getProductById } from '../state/selectors';
import { AppState } from '../state/storeTypes';
import styled from 'styled-components/macro';
import { FormPreviousLink } from 'grommet-icons';
import { loadProducts } from 'state/products/actions';
import { useEffect } from 'react';

import ProductFullInfo from '../components/ProductFullInfo';

const Container = styled(Box)`
  margin: 10px auto 0;
`;

const BackLink = styled(Link)`
  display: flex;
  text-decoration: none;
  padding: 4px 12px;
`;

interface IProductPageProps {
  match: {
    params: { id: string }
  }
};

const ProductPage: React.FC<IProductPageProps> = ({ match }) => {
  const dispatch = useDispatch();
  const product = useSelector((state: AppState) => {
    return getProductById(state, { id: match.params.id });
  });

  useEffect(() => {
    if (!product) {
      dispatch(loadProducts());
    }
  }, [product, dispatch]);

  return (
    <Box fill background="light-5" css={` min-height: 870px; `}>
      <Container>
        <Box background="light-1" margin={{ bottom: '10px' }} elevation="small">
          <BackLink to="/">
            <FormPreviousLink color='brand' />
            <Text
              color="brand"
              css={` line-height: 26px; `}
            >
              Back to all products
            </Text>
          </BackLink>
        </Box>
        <Box background="light-1" elevation="small">
          {!product && (
            <Text color="brand" margin="large">
              Product data is loading or not exist...
            </Text>
          )}
          {product && <ProductFullInfo product={product} />}
        </Box>
      </Container>
    </Box>
  );
};

export default ProductPage;
