import * as React from 'react';
import styled from 'styled-components/macro';
import { Box } from 'grommet';

import ProductsViewer from '../components/ProductsViewer';
import PaginationControl from '../components/PaginationControl';
import Filters from '../components/Filters';
import SearchField from '../components/SearchField';

const Container = styled(Box)`
  margin: 10px auto;
  max-width: 1150px;
`;

const MainColumn = styled(Box)`
  width: 75%;
  padding-top: 24px;
`;

const SideBar = styled(Box)`
  padding-top: 24px;
  width: 25%;
`;

const CatalogPage: React.FC = () => {
  return (
    <Box fill background="light-5" css={` min-height: 920px; `}>
      <Container
        width="xlarge"
        height="full"
        background="light-1"
        flex
        direction="row"
      >
        <SideBar elevation="small" align="center">
          <Filters />
        </SideBar>
        <MainColumn>
          <SearchField />
          <PaginationControl />
          <ProductsViewer />
        </MainColumn>
      </Container>
    </Box>
  );
};

export default CatalogPage;
