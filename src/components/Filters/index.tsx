import React from 'react';
import styled from 'styled-components/macro';

import PriceFilter from './PriceFilter';
import BrandsFilter from './BrandsFilter';
import RatingFilter from './RatingFilter';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const FilterHeader = styled.h3`
  margin: 0 0 8px;
  text-align: center;
`;

const Filters: React.FC = () => {
  return (
    <Form onSubmit={(e) => e.preventDefault()}>
      <FilterHeader>Price range</FilterHeader>
      <PriceFilter />
      <FilterHeader>Rating</FilterHeader>
      <RatingFilter />
      <BrandsFilter />
    </Form>
  );
};

export default Filters;
