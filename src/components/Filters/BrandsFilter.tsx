import React, { memo } from 'react';
import styled from 'styled-components/macro';
import { Box, Button } from 'grommet';
import { AddCircle, FormClose } from 'grommet-icons';
import { useDispatch, useSelector } from 'react-redux';

import { selectBrand, deselectBrand } from 'state/filters/actions';
import { getSelectedBrands, getAvailableBrands } from 'state/selectors';

const SectionHeader = styled.h3`
  margin: 0 0 8px;
  text-align: center;
`;

const BrandBadges = styled(Box)`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 90%;
  max-height: 278px;
  overflow-y: auto;

  margin-bottom: ${(props: { upper?: boolean }) => props.upper && '18px'};
`;

const Badge = styled(Button)`
  width: auto;
  flex-grow: 0;
  padding: 2px 10px 2px 2px;
  margin: 4px;

  svg + div { /* gap between icon and label */
    width: 4px;
  }
`;

const BrandsFilter: React.FC = () => {
  const dispatch = useDispatch();
  const selectedBrands = useSelector(getSelectedBrands);
  const brands = useSelector(getAvailableBrands);

  return (
    <>
      {selectedBrands.length > 0 && (
        <>
          <SectionHeader>Selected Brands</SectionHeader>
          <BrandBadges upper>
            {selectedBrands.map((name: string) => {
                return (
                  <Badge
                    key={name}
                    label={name}
                    icon={<FormClose />}
                    onClick={() => dispatch(deselectBrand(name))}
                  />
                );
              })
            }
          </BrandBadges>
        </>
      )}
      <SectionHeader>
        {selectedBrands.length > 0 ? 'Other Brands' : 'Brands'}
      </SectionHeader>
      <BrandBadges>
        {brands.map((name: string) => {
            return (
              <Badge
                key={name}
                label={name}
                icon={<AddCircle />}
                onClick={() => dispatch(selectBrand(name))}
              />
            );
          })
        }
      </BrandBadges>
    </>
  );
};

export default memo(BrandsFilter);
