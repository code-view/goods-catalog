import React, { memo } from 'react';
import styled from 'styled-components/macro';
import { Box, RangeSelector, Text } from 'grommet';
import { useDispatch, useSelector } from 'react-redux';

import { setRatingRange } from 'state/filters/actions';
import { getRatingRange } from 'state/selectors';
import { MIN_RATE, MAX_RATE } from 'config';

const RangeWithScale = styled.div`
  width: 80%;
  margin-bottom: 28px;
`;

const RatingScale = styled(Box)`
  width: 100%;

  div {
    padding: 0;
  }
`;

const RangePicker = styled(RangeSelector)`
  height: 40px;
  margin: 0 4px;
`;

const createRatingScale = (min: number, max: number) => {
  return Array(max + 1).fill(null).map((_, i) => i + min);
};

const RatingFilter: React.FC = () => {
  const dispatch = useDispatch();
  // max 4, +1 and -1 because of some strange range picker behavior
  const [rateMin, rateMax] = useSelector(getRatingRange);

  const handleRatingRangeChange = ([min, max]: any) => {
    dispatch(setRatingRange([min, max + 1]));
  };

  return (
    <RangeWithScale>
      <RatingScale direction="row" justify="between">
        {createRatingScale(MIN_RATE, MAX_RATE).map(value => (
          <Box key={value} pad="small" border={false}>
            <Text style={{ fontFamily: 'monospace' }}>
              {value}
            </Text>
          </Box>
        ))}
      </RatingScale>
      <RangePicker
        direction="horizontal"
        invert={false}
        min={0}
        max={4}
        step={1}
        round="small"
        values={[rateMin, rateMax - 1]}
        onChange={handleRatingRangeChange}
      />
    </RangeWithScale>
  );
};

export default memo(RatingFilter);
