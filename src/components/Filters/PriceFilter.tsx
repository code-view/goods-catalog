import React, { useState, memo } from 'react';
import styled from 'styled-components/macro';
import { TextInput } from 'grommet';
import { useSelector, useDispatch } from 'react-redux';

import { getPriceRange } from 'state/selectors';
import { setPriceRange } from 'state/filters/actions';

const RangeInput = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  margin-bottom: 28px;

  span {
    display: inline-flex;
    align-items: center;
    margin: 0 8px;
    font-weight: bold;
  }
`;

const PriceField = styled.div`
  width: 40%;
`;

const PriceFilter: React.FC = () => {
  const dispatch = useDispatch();
  const [priceFrom, priceTo] = useSelector(getPriceRange);
  const [inputPriceRange, setInputPriceRange] = useState({
    min: String(priceFrom),
    max: String(priceTo),
  });

  const handlePriceFromChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const numValue = Number(value);
    setInputPriceRange({ ...inputPriceRange, min: value });

    if (value.length > 0 && numValue < priceTo && numValue >= 0) {
      dispatch(setPriceRange([numValue, priceTo]));
    }
  };

  const handlePriceToChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const numValue = Number(value);
    setInputPriceRange({ ...inputPriceRange, max: value });

    if (value.length > 0 && numValue > priceFrom  && numValue > 0) {
      dispatch(setPriceRange([priceFrom, numValue]));
    }
  };

  const correctPriceFrom = () => {
    if (inputPriceRange.min !== String(priceFrom)) {
      setInputPriceRange({ ...inputPriceRange, min: String(priceFrom) });
    }
  };
  const correctPriceTo = () => {
    if (inputPriceRange.max !== String(priceTo)) {
      setInputPriceRange({ ...inputPriceRange, max: String(priceTo) });
    }
  };

  return (
    <RangeInput>
      <PriceField>
        <TextInput
          id="price-from"
          type="number"
          min="0"
          value={inputPriceRange.min}
          onChange={handlePriceFromChange}
          onBlur={correctPriceFrom}
        />
      </PriceField>
      <span> - </span>
      <PriceField>
        <TextInput
          id="price-to"
          type="number"
          min="0"
          value={inputPriceRange.max}
          onChange={handlePriceToChange}
          onBlur={correctPriceTo}
        />
      </PriceField>
    </RangeInput>
  );
};

export default memo(PriceFilter);
