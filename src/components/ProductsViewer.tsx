import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { Product } from '../types';
import ProductCard from './ProductCard';
import { loadProducts } from '../state/products/actions';
import { getPageProducts } from '../state/selectors';

interface IProductsViewerProps {}

const ProductsGrid = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-top: 10px;
  margin-left: 8px;
  padding-top: 4px;
  overflow-y: auto;
`;

const ProductsViewer: React.FC<IProductsViewerProps> = () => {
  const dispatch = useDispatch();
  const products = useSelector(getPageProducts);

  useEffect(() => {
    dispatch(loadProducts());
  }, [dispatch]);

  return (
    <ProductsGrid>
      {products.map((product: Product) => {
        return (
          <ProductCard key={product.id} product={product} />
        );
      })}
    </ProductsGrid>
  );
};

export default ProductsViewer;
