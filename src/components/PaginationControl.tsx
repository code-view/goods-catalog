import React from 'react';
import { Button, Box, Text } from 'grommet';
import styled from 'styled-components/macro';
import { useSelector, useDispatch } from 'react-redux';

import { getTotalPages, getCurrentPage } from '../state/selectors';
import {
  goToFirstPage, goToPrevPage, goToNextPage, goToLastPage,
} from '../state/pagination/actions';

const Container = styled(Box)`
  min-height: auto;
  padding: 8px 0;
  flex-grow: 0;
`;

const PageButton = styled(Button)`
  height: 28px;
  padding: 0 12px;
  margin: 0 4px;
`;

const PageCount = styled.div`
  height: 28px;
  margin: 0 12px;

  span {
    display: inline-block;
    height: 28px;
    line-height: 28px;
    margin: 0 4px;
  }
`;

interface IPaginationControlProps {}

const PaginationControl: React.FC<IPaginationControlProps> = () => {
  const page: number = useSelector(getCurrentPage);
  const totalPages: number = useSelector(getTotalPages);
  const dispatch = useDispatch();

  return (
    <Container flex direction="row" justify="center">
      <PageButton
        label="First"
        disabled={page === 1}
        onClick={() => dispatch(goToFirstPage())}
      />
      <PageButton
        label="Prev"
        disabled={page === 1}
        onClick={() => dispatch(goToPrevPage())}
      />
      <PageCount>
        <Text weight={500}>{page}</Text>
        <Text size="medium"> of </Text>
        <Text weight={500}>{totalPages}</Text>
      </PageCount>
      <PageButton
        label="Next"
        disabled={page === totalPages}
        onClick={() => dispatch(goToNextPage(totalPages))}
      />
      <PageButton
        label="Last"
        disabled={page === totalPages}
        onClick={() => dispatch(goToLastPage(totalPages))}
      />
    </Container>
  );
};

export default PaginationControl;