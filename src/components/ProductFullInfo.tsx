import React from 'react';
import styled from 'styled-components/macro';
import { Image, Text, Heading, Paragraph } from 'grommet';

import { Product } from 'types';

const Details = styled.div`
  text-align: center;
`;

interface IProductFullInfoProps {
  product: Product,
}

const ProductFullInfo: React.FC<IProductFullInfoProps> = ({ product }) => {
  return (
    <>
      <Heading
        level="1"
        size="xsmall"
        margin="small"
        textAlign="center"
      >
        {product.name}
      </Heading>
      <Image
        src={product.img}
        fit="contain"
        css={` margin: 0 16px; `}
      />
      <Details>
        <Paragraph>
          Brand: <Text color="brand">{product.brand}</Text>
        </Paragraph>
        <Paragraph>
          Rate: <Text color="brand" weight="bold">{product.rate}</Text>
        </Paragraph>
        <Paragraph>
          Price:
          {' '}
          <Text color="brand" weight="bold">
            ${product.cost}
          </Text>
        </Paragraph>
      </Details>
    </>
  );
};

export default ProductFullInfo;
