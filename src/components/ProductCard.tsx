import React, { useCallback, memo } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Box, Image, Text } from 'grommet';

import { Product } from '../types';

const Card = styled(Box)`
  padding: 8px;
  margin: 0 8px 16px;
  cursor: pointer;
`;

const Title = styled.h3`
  margin: 8px 0 4px;
  font-weight: 500;
`;

const BottomLine = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Txt = styled(Text)`
  font-size: 16px;
`;

interface IProductCardProps extends RouteComponentProps<any> {
  product: Product,
}

const ProductCard: React.FC<IProductCardProps> = memo((props) => {
  const { product, history } = props;
  const handleCardClick = useCallback(
    () => history.push(`/products/${product.id}`),
    [history, product],
  );

  return (
    <Card
      height="260px"
      width="262px"
      elevation="small"
      onClick={handleCardClick}
    >
      <Image fit="cover" src={product.imgSmall} />
      <Title>
        {product.name}
      </Title>
      <div>
        <Txt>Brand: </Txt>
        <Txt color="brand">
          {product.brand}
        </Txt>
      </div>
      <BottomLine>
        <span>
          <Txt>Rate: </Txt>
          <Txt color="brand" weight="bold">
            {product.rate}
          </Txt>
        </span>
        <span>
          <Txt>Price: </Txt>
          <Txt color="brand" weight="bold">
            ${product.cost}
          </Txt>
        </span>
      </BottomLine>
    </Card>
  );
});

export default withRouter(ProductCard);
