import React from 'react';
import { TextInput } from 'grommet';
import styled from 'styled-components/macro';
import { useDispatch, useSelector } from 'react-redux';

import { getNameSearch } from '../state/selectors';
import { setNameSearch } from '../state/filters/actions';

const Container = styled.div`
  padding-bottom: 14px;
  width: 70%;
  margin: 0 auto;
`;

const SearchField: React.FC = () => {
  const dispatch = useDispatch();
  const searchText = useSelector(getNameSearch);

  return (
    <Container>
      <TextInput
        placeholder="Type product name here to search"
        value={searchText}
        onChange={event => dispatch(setNameSearch(event.target.value))}
      />
    </Container>
  );
};

export default SearchField;
