export const ITEMS_PER_PAGE = 9;

export const MIN_RATE = 0;

export const MAX_RATE = 5;

export const INIT_MAX_PRICE = 1000;
