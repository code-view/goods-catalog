import { createAction } from "redux-starter-kit";

import * as aT from '../actionTypes';

export const setNameSearch = createAction(aT.SET_NAME_SEARCH);

export const setRatingRange = createAction(aT.SET_RATING_RANGE);

export const setPriceRange = createAction(aT.SET_PRICE_RANGE);

export const selectBrand = createAction(aT.SELECT_BRAND);
export const deselectBrand = createAction(aT.DESELECT_BRAND);
