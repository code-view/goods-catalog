import { createReducer } from 'redux-starter-kit';

import { MIN_RATE, MAX_RATE } from 'config';

import * as aT from '../actionTypes';

export interface FiltersState {
  search: string,
  priceRange: [number, number | null],
  rating: [number, number],
  brands: string[],
}

const initialState: FiltersState = {
  search: '',
  priceRange: [0, null],
  rating: [MIN_RATE, MAX_RATE],
  brands: [],
};

const filters = createReducer(initialState, {
  [aT.SET_NAME_SEARCH]: (state, action) => {
    return {
      ...state,
      search: action.payload,
    };
  },

  [aT.SET_RATING_RANGE]: (state, action) => {
    return {
      ...state,
      rating: action.payload,
    };
  },

  [aT.SET_PRICE_RANGE]: (state, action) => {
    return {
      ...state,
      priceRange: action.payload,
    };
  },

  [aT.SELECT_BRAND]: (state, action) => {
    return {
      ...state,
      brands: state.brands.concat(action.payload),
    };
  },

  [aT.DESELECT_BRAND]: (state, action) => {
    return {
      ...state,
      brands: state.brands.filter(brand => brand !== action.payload),
    };
  },
});

export default filters;
