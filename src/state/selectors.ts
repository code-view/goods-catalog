import { createSelector } from "redux-starter-kit";
import { compose } from 'redux';

import { Product } from "types";
import { ITEMS_PER_PAGE, INIT_MAX_PRICE } from 'config';
import { AppState } from "./storeTypes";

export const getProductsById = (state: AppState) => state.products;

export const getProductById = (state: AppState, { id }: { id: string }) => {
  return getProductsById(state)[id];
};

export const getCurrentPage = (state: AppState) => state.pagination.page;

export const getNameSearch = (state: AppState) => state.filters.search;

export const getRatingRange = (state: AppState) => state.filters.rating;

export const getInitPriceRange = (state: AppState) => state.filters.priceRange;

export const getSelectedBrands = (state: AppState) => state.filters.brands;

export const getProductsList = createSelector(
  [getProductsById],
  (productsById) => Object.values(productsById),
);

export const getProductBrands = createSelector(
  [getProductsList],
  (products) => products.reduce((brands: string[], { brand }: Product) => {
    return brands.includes(brand) ? brands : brands.concat(brand);
  }, [])
);

export const getAvailableBrands = createSelector(
  [getProductBrands, getSelectedBrands],
  (brands, selectedBrands) => {
    return brands.filter((brand: string) => !selectedBrands.includes(brand));
  }
);

export const getProductsMaxPrice = createSelector(
  [getProductsList],
  (allProducts) => allProducts.reduce((prevMax: number, product: Product) => {
    const price = product.cost;
    return price > prevMax ? price : prevMax;
  }, INIT_MAX_PRICE)
);

export const getPriceRange = createSelector(
  [getInitPriceRange, getProductsMaxPrice],
  ([priceFrom, priceTo], productsMaxPrice) => {
    const nextPriceTo = priceTo !== null ? priceTo : productsMaxPrice;
    return [priceFrom, nextPriceTo];
  }
);

const filterBySelectedBrands = (brands: string[]) => (products: Product[]) => {
  if (brands.length < 1) return products;
  return products.filter(product => brands.includes(product.brand));
};

const filterBySearchedName = (search: string) => (products: Product[]) => {
  return products.filter(product => product.name.includes(search));
};

const filterByPriceRange = ([from, to]: [number, number]) => (
  (products: Product[]) => {
    return products.filter(({ cost }) => (cost >= from && cost <= to));
  }
);

const filterByRatingRange = ([min, max]: [number, number]) => (
  (products: Product[]) => {
    return products.filter(({ rate }) => (rate >= min && rate <= max));
  }
);

export const getFilteredProducts = createSelector(
  [
    getProductsList,
    getSelectedBrands,
    getNameSearch,
    getPriceRange,
    getRatingRange,
  ],
  (allProducts, selectedBrands, search, priceRange, rating) => {
    const filteredProducts = compose(
      filterBySelectedBrands(selectedBrands),
      filterBySearchedName(search.trim()),
      filterByPriceRange(priceRange),
      filterByRatingRange(rating),
    )(allProducts);
    return filteredProducts;
  },
);

export const getPageProducts = createSelector(
  [getCurrentPage, getFilteredProducts],
  (currentPage: number, products: Array<Product>) => {
    const from = ITEMS_PER_PAGE * (currentPage - 1);
    const to = from + ITEMS_PER_PAGE;
    return products.slice(from, to);
  }
);

export const getTotalPages = (state: AppState) => {
  const filteredProducts = getFilteredProducts(state);
  return filteredProducts.length === 0
    ? 1
    : Math.ceil(filteredProducts.length / ITEMS_PER_PAGE);
};
