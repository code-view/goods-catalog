import { createAction } from 'redux-starter-kit';
import { Dispatch } from 'redux';

import * as aT from '../actionTypes';
import api from '../../lib/api';
import { normalizeProducts } from '../../lib/normalize';

export const loadProductsStart = createAction(aT.LOAD_PRODUCTS_START);
export const loadProductsDone = createAction(aT.LOAD_PRODUCTS_DONE);
export const loadProductsFail = createAction(aT.LOAD_PRODUCTS_FAIL);

export const loadProducts = () => async (dispatch: Dispatch) => {
  dispatch(loadProductsStart());
  try {
    const { products } = api.fetchProducts();
    const payload = normalizeProducts(products);
    dispatch(loadProductsDone(payload));
  } catch (e) {
    dispatch(loadProductsFail({ message: e.message }));
  }
};
