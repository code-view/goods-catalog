import { createReducer } from 'redux-starter-kit';

import * as aT from '../actionTypes';
import { Product } from 'types';

export interface ProductsState {
  [productId: string]: Product,
}

const productsReducer = createReducer({}, {
  [aT.LOAD_PRODUCTS_DONE]: (state, action) => {
    return { ...state, ...action.payload.byId };
  },
});

export default productsReducer;
