import { FiltersState } from './filters/reducer';
import { ProductsState } from './products/reducer';
import { PaginState } from './pagination/reducer';

export interface AppState {
  filters: FiltersState,
  products: ProductsState,
  pagination: PaginState,
};