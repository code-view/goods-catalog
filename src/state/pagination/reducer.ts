import { createReducer } from 'redux-starter-kit';

import * as aT from '../actionTypes';

export interface PaginationState {
  page: number,
}

const initialState: PaginationState = {
  page: 1,
};

const setFirstPage = (state: PaginationState) => ({ ...state, page: 1 });

const pagination = createReducer(initialState, {
  [aT.GO_TO_FIRST_PAGE]: setFirstPage,
   // next several are added to reset current page on filter changes
  [aT.SET_NAME_SEARCH]: setFirstPage,
  [aT.SET_RATING_RANGE]: setFirstPage,
  [aT.SET_PRICE_RANGE]: setFirstPage,
  [aT.SELECT_BRAND]: setFirstPage,
  [aT.DESELECT_BRAND]: setFirstPage,

  [aT.GO_TO_LAST_PAGE]: (state, action) => ({
    ...state,
    page: action.payload,
  }),

  [aT.GO_TO_PREV_PAGE]: (state) => {
    return state.page > 1 
      ? { ...state, page: state.page - 1 }
      : state;
  },

  [aT.GO_TO_NEXT_PAGE]: (state, action) => {
    return state.page < action.payload
      ? { ...state, page: state.page + 1 }
      : state;
  },
});

export default pagination;