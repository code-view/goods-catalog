import { createAction } from "redux-starter-kit";

import * as aT from '../actionTypes';

export const goToNextPage = createAction(aT.GO_TO_NEXT_PAGE);
export const goToPrevPage = createAction(aT.GO_TO_PREV_PAGE);
export const goToFirstPage = createAction(aT.GO_TO_FIRST_PAGE);
export const goToLastPage = createAction(aT.GO_TO_LAST_PAGE);
