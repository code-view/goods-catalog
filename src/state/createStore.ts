import { configureStore, getDefaultMiddleware } from 'redux-starter-kit';

import rootReducer from './rootReducer';

const isDevelopment = process.env.NODE_ENV === 'development';

export default function createStore(preloadedState = {}) {
  const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware(),
    preloadedState,
    devTools: isDevelopment,
  });
  return store;
}
