import { combineReducers } from 'redux';

import filters from './filters/reducer';
import products from './products/reducer';
import pagination from './pagination/reducer';

export default combineReducers({
  filters,
  products,
  pagination,
});