/* eslint-disable no-restricted-imports */
import { CSSProp } from "styled-components";

declare module "styled-components/macro" {
  export * from "styled-components";
  import styled from "styled-components";
  export default styled;
}

declare module "react" {
  interface HTMLAttributes<T> extends DOMAttributes<T> {
    css?: CSSProp;
  }
}

export interface Product {
  id: string,
  name: string,
  cost: number,
  img: string,
  imgSmall: string,
  rate: number,
  brand: string
}
