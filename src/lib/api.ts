import { Product } from 'types';

import productsData from '../products.json';

type SortType = 'price-asc' | 'price-desc' | 'rate-asc' | 'rate-desc';

interface FetchOptions {
  sortBy?: SortType,
}

const sortProducts = (sortBy: SortType) => {
  const byPrice = sortBy.includes('price');

  return (curr: Product, next: Product) => {
    if (byPrice) {
      return sortBy === 'price-asc'
        ? curr.cost - next.cost
        : next.cost - curr.cost;
    }
    return sortBy === 'rate-asc'
      ? curr.rate - next.rate
      : next.rate - curr.rate;
  };
};

const api = {
  fetchProducts({ sortBy = 'rate-desc' }: FetchOptions = {}) {
    // make sorting here to simulate already sorted data from backend
    productsData.products.sort(sortProducts(sortBy));
    return productsData;
  },
};

export default api;