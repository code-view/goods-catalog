import { Product } from "types";

interface ProductsPayload {
  ids: string[],
  byId: { [id: string]: Product }
};

export const normalizeProducts = (products: Array<Product>) => {
  return products.reduce((acc: ProductsPayload, product: Product) => {
    acc.ids.push(product.id);
    acc.byId[product.id] = product;
    return acc;
  }, { ids: [], byId: {} });
};
