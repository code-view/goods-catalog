import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import CatalogPage from './pages/CatalogPage';
import ProductPage from './pages/ProductPage';

const App: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={CatalogPage} />
        <Route path="/products/:id" component={ProductPage} />
        <Redirect to="/" />
      </Switch>
    </Router>
  );
};

export default App;
