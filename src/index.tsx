import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Grommet } from 'grommet';
import App from './App';
import GlobalStyle from './GlobalStyle';
import * as serviceWorker from './serviceWorker';

import createStore from './state/createStore';

const store = createStore();

const theme = {
  global: {
    font: {
      family: 'Roboto',
      size: '14px',
      height: '20px',
    },
  },
};

ReactDOM.render(
  (
    <Provider store={store}>
      <GlobalStyle />
      <Grommet theme={theme} full>
        <App />
      </Grommet>
    </Provider>
  ),
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
